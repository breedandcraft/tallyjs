"use strict";

const assert = require("chai").assert;
const Redis = require("ioredis");
const Tally = require("../index");

const redisConfig = {
	host: "192.168.99.100",
	port: 6379,
	db: 9,
	keyPrefix: "tally:"
};

const tally = new Tally(redisConfig);
const redis = Redis(redisConfig);

function flushRedis(done) {
	redis.flushdb(() => {
		done();
	});
}

// [NOTE] These are just some very basic functional tests to ensure that it
// works at a basic level. If any of these fail then something is definitely
// wrong.

describe("Benchmark addCounter", () => {
	after(flushRedis);

	it("10,000 calls", done => {
		const promises = [];
		const data = {
			key: "testKey",
			count: 1
		};

		const start = Date.now();

		let index = 0;
		while (index < 10000) {
			index += 1;
			promises.push(tally.addCounter(data));
		}

		Promise.all(promises).then(() => {
			const duration = Date.now() - start;
			const perOperation = duration / 10000;

			console.log(duration, perOperation);

			done();
		});
	});
});

describe("Benchmark addValue", () => {
	after(flushRedis);

	it("10,000 calls", done => {
		const promises = [];
		const data = {
			key: "testKey",
			count: 1,
			value: 1
		};

		const start = Date.now();

		let index = 0;
		while (index < 10000) {
			index += 1;
			promises.push(tally.addValue(data));
		}

		Promise.all(promises).then(() => {
			const duration = Date.now() - start;
			const perOperation = duration / 10000;

			console.log(duration, perOperation);
			
			done();
		});
	});
});

describe("addCounter, getCounterSum and getCounterRange functions", () => {
	before(done => {
		const data = {
			key: "testKey",
			timestamp: "2016-03-01T00:00:00Z"
		};

		tally.addCounter(data).then(() => {
			done();
		});
	});

	after(flushRedis);

	it("gets sum using getCounterSum", done => {
		const query = {
			key: "testKey"
		};

		tally.getCounterSum(query).then(res => {
			assert(res.count === 1);
			done();
		});
	});

	it("gets a count of 0 if key does not exist", done => {
		const query = {
			key: "testKeyNoExist"
		};

		tally.getCounterSum(query).then(res => {
			assert(res.count === 0);
			done();
		});
	});

	it("gets array using getCounterRange", done => {
		const query = {
			key: "testKey"
		};

		tally.getCounterRange(query).then(res => {
			assert(res.values[0].count === 1);
			done();
		});
	});

	it("gets object using getCounterRange", done => {
		const query = {
			key: "testKey",
			resultType: "object"
		};

		tally.getCounterRange(query).then(res => {
			assert(res.values["2016-03-01T00:00:00Z"] === 1);
			done();
		});
	});

	it("gets an empty values array if key does not exist", done => {
		const query = {
			key: "testKeyNoExist"
		};

		tally.getCounterRange(query).then(res => {
			assert(res.values.length === 0);
			done();
		});
	});
});

describe("addValue, getValueSum and getValueRange functions", () => {
	before(done => {
		const data = {
			key: "testKey",
			count: 10,
			value: 11,
			timestamp: "2016-03-01T00:00:00Z"
		};

		tally.addValue(data).then(res => {
			done();
		});
	});

	after(flushRedis);

	it("gets sum using getValueSum", done => {
		const query = {
			key: "testKey"
		};

		tally.getValueSum(query).then(res => {
			assert(res.count === 10);
			assert(res.sum === 110);
			assert(res.average === 11);
			done();
		});
	});

	it("gets a count, sum and average of 0 if key does not exist", done => {
		const query = {
			key: "testKeyNoExist"
		};

		tally.getValueSum(query).then(res => {
			assert(res.count === 0);
			assert(res.sum === 0);
			assert(res.average === 0);
			done();
		});
	});

	it("gets array using getValueRange", done => {
		const query = {
			key: "testKey"
		};

		tally.getValueRange(query).then(res => {
			assert(res.values[0].count === 10);
			assert(res.values[0].sum === 110);
			assert(res.values[0].average === 11);
			done();
		});
	});

	it("gets object using getValueRange", done => {
		const query = {
			key: "testKey",
			resultType: "object"
		};

		tally.getValueRange(query).then(res => {
			assert(res.values["2016-03-01T00:00:00Z"].count === 10);
			assert(res.values["2016-03-01T00:00:00Z"].sum === 110);
			assert(res.values["2016-03-01T00:00:00Z"].average === 11);
			done();
		});
	});

	it("gets an empty values array if key does not exist", done => {
		const query = {
			key: "testKeyNoExist"
		};

		tally.getValueRange(query).then(res => {
			assert(res.values.length === 0);
			done();
		});
	});
});