"use strict";

const assert = require("chai").assert;
const moment = require("moment");

const validationUtil = require("../../utils/validation.js");

describe("validateData function", () => {
	it("throws an error if data.key is undefined", done => {
		const data = {};

		assert.throws(() => validationUtil.validateData(data), Error);
		
		done();
	});

	it("throws an error if data.key is an empty string", done => {
		const data = {
			key: ""
		};

		assert.throws(() => validationUtil.validateData(data), Error);
		
		done();
	});

	it("throws an error if data.interval is defined and not an integer", done => {
		const data = [
			{
				key: "test",
				interval: "12"
			},
			{
				key: "test",
				interval: []
			},
			{
				key: "test",
				interval: {}
			},
			{
				key: "test",
				interval: 123.45
			}
		];

		data.forEach(item => {
			assert.throws(() => validationUtil.validateData(item), Error);
		});

		done();
	});

	it("does not throw an error if data.interval is an integer", done => {
		const data = {
			key: "test",
			interval: 12345
		};

		assert.doesNotThrow(() => validationUtil.validateData(data), Error);

		done();
	});

	it("does not throw an error if data has no issues", done => {
		const data = {
			key: "test"
		};

		assert.doesNotThrow(() => validationUtil.validateData(data), Error);
		
		done();
	});

	it("sets interval to 3600 if interval undefined", done => {
		const data = {
			key: "test"
		};

		validationUtil.validateData(data);
		assert(data.interval === 3600);

		done();
	});

	it("sets interval to 3600 if interval less than or equal to 0", done => {
		const data = [
			{
				key: "test",
				interval: 0
			},
			{
				key: "test",
				interval: -1
			}
		];

		data.forEach(item => {
			validationUtil.validateData(item);
			assert(item.interval === 3600);
		});
		
		done();
	});

	it("does not alter interval if greater than 0", done => {
		const data = [
			{
				data: {
					key: "test",
					interval: 1
				},
				expect: 1
			},
			{
				data: {
					key: "test",
					interval: 99999999
				},
				expect: 99999999
			},
		];

		data.forEach(item => {
			validationUtil.validateData(item.data);
			assert(item.data.interval === item.expect);
		});
		
		done();
	});
});

describe("validateInput function", () => {
	it("throws an error if data.timestamp is invalid", done => {
		const data = [
			{
				key: "test",
				timestamp: -1
			},
			{
				key: "test",
				timestamp: "notadate"
			},
		];

		data.forEach(item => {
			assert.throws(() => validationUtil.validateInput(item), Error);
		});
		
		done();
	});

	it("sets timestamp to now if timestamp undefined", done => {
		// [NOTE] We've set the interval to 1 day to attempt to prevent an issue
		// where the test may be ran on the change of the hour and the
		// nano-seconds that the function takes to run will cause the test to
		// fail.
		const data = {
			key: "test",
			interval: 86400
		};
		const now = moment().startOf("day").utc().unix();

		validationUtil.validateInput(data);
		assert(data.timestamp === now);
		
		done();
	});

	it("sets timestamp to a floored value", done => {
		const data = {
			key: "test",
			timestamp: "2016-02-19T03:26:07Z"
		};

		validationUtil.validateInput(data);
		assert(data.timestamp === 1455850800);
		
		done();
	});

	it("sets count to 1 if it is undefined", done => {
		const data = {
			key: "test"
		};

		validationUtil.validateInput(data);
		assert(data.count === 1);
		
		done();
	});

	it("does not alter count if not undefined", done => {
		const data = [
			{
				data: {
					key: "test",
					count: 0
				},
				expect: 0
			},
			{
				data: {
					key: "test",
					count: 1
				},
				expect: 1
			},
			{
				data: {
					key: "test",
					count: 90909
				},
				expect: 90909
			},
			{
				data: {
					key: "test",
					count: -1
				},
				expect: -1
			},
			{
				data: {
					key: "test",
					count: -999999
				},
				expect: -999999
			},
		];

		data.forEach(item => {
			validationUtil.validateInput(item.data);
			assert(item.data.count === item.expect);
		});
		
		done();
	});

	it("sets value to 1 if it is undefined and is a value", done => {
		const data = {
			type: "v",
			key: "test"
		};

		validationUtil.validateInput(data);
		assert(data.value === 1);
		
		done();
	});

	it("does not alter value if not undefined and is a value", done => {
		const data = [
			{
				data: {
					type: "v",
					key: "test",
					value: 0
				},
				expect: 0
			},
			{
				data: {
					type: "v",
					key: "test",
					value: 1
				},
				expect: 1
			},
			{
				data: {
					type: "v",
					key: "test",
					value: 90909
				},
				expect: 90909
			},
			{
				data: {
					type: "v",
					key: "test",
					value: -1
				},
				expect: -1
			},
			{
				data: {
					type: "v",
					key: "test",
					value: -999999
				},
				expect: -999999
			},
		];

		data.forEach(item => {
			validationUtil.validateInput(item.data);
			assert(item.data.value === item.expect);
		});
		
		done();
	});
});

describe("validateQuery function", () => {
	it("throws an error if data.start is invalid", done => {
		const data = [
			{
				key: "test",
				start: -1
			},
			{
				key: "test",
				start: "notadate"
			},
		];

		data.forEach(item => {
			assert.throws(() => validationUtil.validateQuery(item), Error);
		});
		
		done();
	});

	it("throws an error if data.end is invalid", done => {
		const data = [
			{
				key: "test",
				end: -1
			},
			{
				key: "test",
				end: "notadate"
			},
		];

		data.forEach(item => {
			assert.throws(() => validationUtil.validateQuery(item), Error);
		});
		
		done();
	});

	it("converts start to unix", done => {
		const data = {
			key: "test",
			start: "2016-02-19T03:26:07Z"
		};

		validationUtil.validateQuery(data);

		assert(data.start === 1455852367);

		done();
	});

	it("converts end to unix", done => {
		const data = {
			key: "test",
			end: "2016-02-19T03:26:00Z"
		};

		validationUtil.validateQuery(data);

		assert(data.end === 1455852360);

		done();
	});
});