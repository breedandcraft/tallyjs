"use strict";

const assert = require("chai").assert;

const timeUtil = require("../../utils/time.js");

describe("validateTimestamp function", () => {
	it("throws an error if timestamp is an empty string", done => {
		const timestamp = "";

		assert.throws(() => timeUtil.validateTimestamp(timestamp), Error);
		
		done();
	});

	it("throws an error if timestamp is a string in an incorrect format", done => {
		const timestamps = [
			"2016-02-19T03:26Z",
			"2016-02-19",
			"2016-02-19T03:26:00"
		];

		timestamps.forEach(timestamp => {
			assert.throws(() => timeUtil.validateTimestamp(timestamp), Error);
		});
		
		done();
	});

	it("throws an error if timestamp is less than 0", done => {
		const timestamp = -1;

		assert.throws(() => timeUtil.validateTimestamp(timestamp), Error);
		
		done();
	});

	it("does not throw an error if timestamp is ISO 8061", done => {
		const timestamp = "2016-02-19T03:26:02Z";

		assert.doesNotThrow(() => timeUtil.validateTimestamp(timestamp), Error);
		
		done();
	});

	it("does not throw an error if timestamp is postitive number", done => {
		const timestamp = 1455850800;

		assert.doesNotThrow(() => timeUtil.validateTimestamp(timestamp), Error);
		
		done();
	});
});

describe("floorTimestamp function", () => {
	it("returns undefined if timestamp is undefined", done => {
		const res = timeUtil.floorTimestamp(
			undefined,
			3600
		);

		assert(res === undefined);

		done();
	});
	
	it("correctly floors for a variety of timestamps and intervals", done => {
		const data = [
			{	
				data: {
					timestamp: 1455852369,
					interval: 5
				},
				expects: 1455852365
			},
			{	
				data: {
					timestamp: 1455852367,
					interval: 60
				},
				expects: 1455852360
			},
			{	
				data: {
					timestamp: 1455852367,
					interval: 3600
				},
				expects: 1455850800
			},
			{	
				data: {
					timestamp: 1455852367,
					interval: 86400
				},
				expects: 1455840000
			},
		];

		data.forEach(item => {
			const res = timeUtil.floorTimestamp(
				item.data.timestamp,
				item.data.interval
			);
			assert(res === item.expects);
		});

		done();
	});
});

describe("getTimespace function", () => {
	it("correctly floors for a variety of timestamps,  intervals and hash items", done => {
		const data = [
			{	
				data: {
					timestamp: 1455852369,
					interval: 5
				},
				expects: [
					1455851520,
					1455851520
				]
			},
			{	
				data: {
					timestamp: 1455852367,
					interval: 60
				},
				expects: [
					1455851520,
					1455851520
				]
			},
			{	
				data: {
					timestamp: 1455852367,
					interval: 3600
				},
				expects: [
					1454284800,
					1455206400
				]
			},
			{	
				data: {
					timestamp: 1455852367,
					interval: 86400
				},
				expects: [
					1415577600,
					1437696000
				]
			},
		];

		data.forEach(item => {
			const res = timeUtil.getTimespace(
				item.data.timestamp,
				item.data.interval,
				512
			);
			assert(res === item.expects[0]);
		});

		data.forEach(item => {
			const res = timeUtil.getTimespace(
				item.data.timestamp,
				item.data.interval,
				256
			);
			assert(res === item.expects[1]);
		});

		done();
	});
});

describe("getTimepoint function", () => {
	it("throws an error if timespace is less than timestamp", done => {
		assert.throws(
			() => timeUtil.getTimepoint(
				0,
				1,
				1
			),
			Error
		);
		done();
	});

	it("does not throw an error if timespace is equal to timestamp", done => {
		assert.doesNotThrow(
			() => timeUtil.getTimepoint(
				0,
				0,
				1
			),
			Error
		);
		done();
	});

	it("correctly floors for a variety of timestamps, timespaces and intervals", done => {
		const data = [
			{	
				data: {
					timestamp: 1455852565,
					timespace: 1455851520,
					interval: 5
				},
				expects: 209
			},
			{	
				data: {
					timestamp: 1455851520,
					timespace: 1455851520,
					interval: 60
				},
				expects: 0
			},
			{	
				data: {
					timestamp: 1454727600,
					timespace: 1454284800,
					interval: 3600
				},
				expects: 123
			},
			{	
				data: {
					timestamp: 1454976000,
					timespace: 1415577600,
					interval: 86400
				},
				expects: 456
			},
		];

		data.forEach(item => {
			const res = timeUtil.getTimepoint(
				item.data.timestamp,
				item.data.timespace,
				item.data.interval
			);

			assert(res === item.expects);
		});

		done();
	});
});

describe("resolveTimepoint function", () => {
	it("returns correct timestamps for a range of timespaces, timepoints and intervals", done => {
		const data = [
			{	
				data: {
					timespace: 1455851520,
					timepoint: 60,
					interval: 5
				},
				expects: 1455851820
			},
			{	
				data: {
					timespace: 1455851520,
					timepoint: 111,
					interval: 111
				},
				expects: 1455863841
			},
			{	
				data: {
					timespace: 1454284800,
					timepoint: 501,
					interval: 3600
				},
				expects: 1456088400
			},
			{	
				data: {
					timespace: 1415577600,
					timepoint: 333,
					interval: 86400
				},
				expects: 1444348800
			},
		];

		data.forEach(item => {
			const res = timeUtil.resolveTimepoint(
				item.data.timespace,
				item.data.timepoint,
				item.data.interval
			);

			assert(res === item.expects);
		});

		done();
	});
});

describe("checkTimeIsInRange function", () => {
	it("returns correct answer for multiple ranges and timestamps", done => {
		const data = [
			{	
				data: {
					start: 60,
					end: 12345
				},
				timestamp: 5,
				expects: false
			},
			{	
				data: {
					start: 60,
					end: 12345
				},
				timestamp: 60,
				expects: true
			},
			{	
				data: {
					start: 0,
					end: 12345
				},
				timestamp: 123456,
				expects: false
			},
			{	
				data: {
					start: undefined,
					end: 60
				},
				timestamp: 5,
				expects: true
			},
			{	
				data: {
					start: 60,
					end: undefined
				},
				timestamp: 5,
				expects: false
			},
			{	
				data: {
					start: undefined,
					end: undefined
				},
				timestamp: 12345,
				expects: true
			},
			{	
				data: {
					start: undefined,
					end: 1456128000
				},
				timestamp: 145612800,
				expects: true
			},
		];

		data.forEach(item => {
			const res = timeUtil.checkTimeIsInRange(
				item.data,
				item.timestamp
			);

			assert(res === item.expects);
		});

		done();
	});
});