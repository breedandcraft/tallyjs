"use strict";

const assert = require("chai").assert;
const Redis = require("ioredis");
const RedisStoreUtil = require("../../utils/redisStore");
const timeUtil = require("../../utils/time");

const redisConfig = {
	host: "192.168.99.100",
	port: 6379,
	db: 9,
	keyPrefix: "tally:"
};

const redisStoreUtil = new RedisStoreUtil(redisConfig);
const redis = new Redis(redisConfig);

function flushRedis(done) {
	redis.flushdb(() => {
		done();
	});
}

// [NOTE] We should make this test more robust.
// This test should be split up into addCounter and addValue tests.
// Also need to add addData tests.
describe("addCounter and addValue functions", () => {
	before(done => {
		this.counterData = {
			type: "c",
			key: "testKey",
			timestamp: 1455840000,
			interval: 86400,
			count: 12345
		};

		this.valueData = {
			type: "v",
			key: "testKey",
			timestamp: 1455840000,
			interval: 86400,
			count: 12345,
			value: 10
		};

		this.counterKey = this.counterData.type + this.counterData.key;
		this.valueKey = this.valueData.type + this.valueData.key;

		this.timespace = timeUtil.getTimespace(
			this.counterData.timestamp,
			this.counterData.interval,
			redisStoreUtil.hashMaxZiplistEntries
		);
		
		this.timepoint = timeUtil.getTimepoint(
			this.counterData.timestamp,
			this.timespace,
			this.counterData.interval
		);

		const promises = [];

		promises.push(redisStoreUtil.addCounter(this.counterData));
		promises.push(redisStoreUtil.addValue(this.valueData));

		Promise.all(promises).then(() => {
			done();
		});
	});

	after(flushRedis);

	describe("is counter or value", () => {
		it("adds interval to the keys's interval set", done => {
			const results = [];

			results.push(redis.sismember(this.counterKey, this.counterData.interval).then(result => {
				assert(result === 1);
			}));

			results.push(redis.scard(this.counterKey).then(result => {
				assert(result === 1);
			}));

			Promise.all(results).then(() => {
				done();
			});	

		});

		it("adds timespace to interval's timespace set", done => {
			const results = [];

			const intervalKey = this.counterKey + this.counterData.interval;

			results.push(redis.sismember(intervalKey , this.timespace));

			results.push(redis.scard(intervalKey));

			Promise.all(results).then(results => {
				assert(results[0] === 1);

				assert(results[1] === 1);

				done();
			});	
		});
	});

	describe("is counter", () => {
		it("increments the correct counters", done => {

			const timespaceKey = this.counterKey + this.counterData.interval + this.timespace;

			redis.hget(timespaceKey , this.timepoint).then(result => {
				assert(parseInt(result) === this.counterData.count);

				done();
			});	
		});
	});

	describe("is value", () => {
		it("increments the correct counters", done => {
			const timespaceKey = this.valueKey + this.valueData.interval + this.timespace + "count";

			redis.hget(timespaceKey , this.timepoint).then(results => {
				assert(parseInt(results) === this.valueData.count);

				done();
			});	
		});

		it("increments the correct sums", done => {
			const timespaceKey = this.valueKey + this.valueData.interval + this.timespace + "sum";

			redis.hget(timespaceKey , this.timepoint).then(results => {
				assert(parseInt(results) === (this.valueData.count * this.valueData.value));

				done();
			});
		});
	});
});

// [NOTE] The tests for this function need to be made more robust/test more
// things.
describe("getData function", () => {
	it("rejects if query.result is undefined", done => {
		const query = {};

		redisStoreUtil.getData(query).catch(err => {
			assert(err instanceof Error);
			done();
		});
	});

	it("rejects if query.result is not an object", done => {
		const query = {};

		const results = [
			12345,
			"test"
		];

		const promises = [];
		results.forEach(result => {
			query.result = result;

			redisStoreUtil.getData(query).catch(err => {
				assert(err instanceof Error);
			});
		});
		

		Promise.all(promises).then(() => {
			done();
		});
	});

	it("returns result with error message if key has no data", done => {
		const queries = [
			{
				type: "c",
				key: "test",
				queryType: "range",
				result: {}
			},
			{
				type: "v",
				key: "test",
				queryType: "range",
				result: {}
			},
			{
				type: "c",
				key: "test",
				queryType: "sum",
				result: {}
			},
			{
				type: "v",
				key: "test",
				queryType: "sum",
				result: {}
			},
		];

		Promise.all(queries.map(query => {
			return redisStoreUtil.getData(query).then(result => {
				assert(result.error !== undefined);
			});
		})).then(() => {
			done();
		});
	});
});

describe("getTimespaces function", () => {
	before(done => {
		const promises = [];
		promises.push(redis.sadd(
			"cKey3600",
			1
		));

		promises.push(redis.sadd(
			"cKey3600",
			2
		));

		promises.push(redis.sadd(
			"cKey3600",
			3
		));

		promises.push(redis.sadd(
			"cKey60",
			1
		));

		promises.push(redis.sadd(
			"cKey60",
			2
		));

		Promise.all(promises).then(() => {
			done();
		});
	});

	after(flushRedis);

	it("resolves an array of arrays of timespaces", done => {
		const query = {
			type: "c",
			key: "Key",
		};

		const meta = {
			intervals: [
				"60",
				"3600"
			]
		};

		redisStoreUtil.getTimespaces(query, meta).then(timespaces => {
			assert(timespaces.length === 2);
			assert(timespaces[0].length === 2);
			assert(timespaces[1].length === 3);
			done();
		});
		
	});
});

describe("getTimepoints function", () => {
	describe("if is counter", () => {
		before(done => {
			this.data = {
				type: "c",
				key: "testKey",
				timestamp: 1456185600,
				interval: 3600,
				count: 1
			};

			const promises = [];
			for (let index = 0; index < 1440; index += 1) {
				promises.push(redisStoreUtil.addCounter(this.data));

				this.data.timestamp += this.data.interval;
			}

			Promise.all(promises).then(() => {
				done();
			});
		});

		after(flushRedis);

		it("resolves an array of objects of timepoints limited by various start and ends", done => {
			const baseQuery = {
				type: this.data.type,
				queryType: "range",
				key: this.data.key
			};

			const queries = [
				{
					query: {},
					expects: {
						length: 3,
						timepoints: [
							496,
							512,
							432
						]
					}
				},
				{
					query: {
						end: 1457971199
					},
					expects: {
						length: 1,
						timepoints: [
							496
						]
					}
				},
				{
					query: {
						start: 1457971200
					},
					expects: {
						length: 2,
						timepoints: [
							512,
							432
						]
					}
				},
				{
					query: {
						start: 1457971200,
						end: 1459814399
					},
					expects: {
						length: 1,
						timepoints: [
							512
						]
					}
				},
				{
					query: {
						start: 1457971199,
						end: 1459814399
					},
					expects: {
						length: 2,
						timepoints: [
							496,
							512
						]
					}
				},
			];

			const meta = {
				intervals: [
					3600
				],
				timespaces: [],
				intervalTimespaces: [
					[
						1456128000,
						1457971200,
						1459814400
					]
				]
			};

			const promises = [];
			queries.forEach(query => {
				Object.assign(query.query, baseQuery);

				let promise = redisStoreUtil.getTimepoints(query.query, meta).then(timepoints => {
					assert(timepoints.length === query.expects.length);

					timepoints.forEach((timepointSet, index) => {
						assert(Object.keys(timepointSet).length === query.expects.timepoints[index]);
					});
				});

				promises.push(promise);
			});

			Promise.all(promises).then(() => {
				done();
			});

		});
	});

	describe("if is value", () => {
		before(done => {
			this.data = {
				type: "v",
				key: "testKey",
				timestamp: 1456185600,
				interval: 3600,
				count: 1,
				value: 10
			};

			this.setKey = this.data.type + this.data.key;

			const promises = [];
			for (let index = 0; index < 1440; index += 1) {
				promises.push(redisStoreUtil.addValue(this.data));

				this.data.timestamp += this.data.interval;
			}

			Promise.all(promises).then(() => {
				done();
			});
		});

		after(flushRedis);

		it("resolves an array of objects of limited by various start and ends", done => {
			const baseQuery = {
				type: this.data.type,
				queryType: "range",
				key: this.data.key
			};

			const queries = [
				{
					query: {},
					expects: {
						length: 6,
						timepoints: [
							496,
							496,
							512,
							512,
							432,
							432
						]
					}
				},
				{
					query: {
						end: 1457971199
					},
					expects: {
						length: 2,
						timepoints: [
							496,
							496
						]
					}
				},
				{
					query: {
						start: 1457971200
					},
					expects: {
						length: 4,
						timepoints: [
							512,
							512,
							432,
							432
						]
					}
				},
				{
					query: {
						start: 1457971200,
						end: 1459814399
					},
					expects: {
						length: 2,
						timepoints: [
							512,
							512
						]
					}
				},
				{
					query: {
						start: 1457971200,
						end: 1457971200
					},
					expects: {
						length: 2,
						timepoints: [
							512,
							512
						]
					}
				},
				{
					query: {
						start: 1457971199,
						end: 1459814399
					},
					expects: {
						length: 4,
						timepoints: [
							496,
							496,
							512,
							512
						]
					}
				},
			];

			const meta = {
				intervals: [
					3600
				],
				timespaces: [],
				intervalTimespaces: [
					[
						1456128000,
						1457971200,
						1459814400
					]
				]
			};

			const promises = [];
			queries.forEach(query => {
				Object.assign(query.query, baseQuery);

				let promise = redisStoreUtil.getTimepoints(query.query, meta).then(timepoints => {
					assert(timepoints.length === query.expects.length);

					timepoints.forEach((timepointSet, index) => {
						assert(Object.keys(timepointSet).length === query.expects.timepoints[index]);
					});
				});

				promises.push(promise);
			});

			Promise.all(promises).then(() => {
				done();
			});
		});
	});
});

describe("prepareValueTimepoints function", () => {
	it("combines the counts and sums into a single object", done => {
		const timespaces = [
			{
				0: 1,
				1: 2,
				2: 3,
				10: 11
			},
			{
				0: 11,
				1: 21,
				2: 31,
				10: 111
			},
			{
				0: 5,
				1: 6,
				2: 7,
				10: 16
			},
			{
				0: 111,
				1: 211,
				2: 311,
				10: 1111
			},
			{
				0: 12,
				1: 6,
				2: 7,
				10: 16
			},
			{
				0: 2111,
				1: 211,
				2: 311,
				10: 1111
			}
		];

		const combined = redisStoreUtil.prepareValueTimepoints(timespaces);

		assert(combined[0]["0"].count === 1);
		assert(combined[0]["0"].sum === 11);

		assert(combined[0]["1"].count === 2);
		assert(combined[0]["1"].sum === 21);

		assert(combined[1]["0"].count === 5);
		assert(combined[1]["0"].sum === 111);

		assert(combined[2]["0"].count === 12);
		assert(combined[2]["0"].sum === 2111);

		done();
	});
});

describe("resolveTimepoints function", () => {
	describe("if counter", () => {
		this.timepoints = [
			{
				0: 1,
				1: 1,
				2: 1,
				3: 1,
				4: 1
			},
			{
				0: 1,
				1: 1,
				2: 1,
				3: 1,
				4: 1
			},
			{
				0: 1,
				1: 1,
				2: 1,
				3: 1,
				4: 1
			}
		];

		describe("if sum", () => {
			it("sums the correct value limited by various start and ends", done => {
				const baseQuery = {
					type: "c",
					queryType: "sum"
				};

				const queries = [
					{
						query: {},
						expects: {
							count: 15
						}
					},
					{
						query: {
							end: 1459814400
						},
						expects: {
							count: 11
						}
					},
					{
						query: {
							start: 1459814400
						},
						expects: {
							count: 5
						}
					},
					{
						query: {
							start: 1457971200,
							end: 1459814400
						},
						expects: {
							count: 6
						}
					}
				];

				const meta = {
					intervals: [
						3600
					],
					timespaces: [
						[
							1456128000,
							1457971200,
							1459814400
						]
					],
					timepoints: this.timepoints
				};

				queries.forEach(query => {
					Object.assign(query.query, baseQuery);

					const result = {
						count: 0
					};

					redisStoreUtil.resolveTimepoints(query.query, meta, result);

					assert(result.count === query.expects.count);
				});

				done();
			});
		});

		describe("if range", () => {
			it("returns an array of objects with a timestamp and a count" + 
				"limited by various start and ends at various interval resolutions", done => {
				const baseQuery = {
					type: "c",
					queryType: "range"
				};

				const queries = [
					{
						query: {
							interval: 3600
						},
						expects: {
							length: 15,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T08:00:00Z",
									count: 1
								},
								{	
									index: 7,
									timestamp: "2016-03-14T18:00:00Z",
									count: 1
								},
								{	
									index: 14,
									timestamp: "2016-04-05T04:00:00Z",
									count: 1
								}
							]
						}
					},
					{
						query: {
							interval: 3600,
							start: 1459814400
						},
						expects: {
							length: 5,
							values: [
								{	
									index: 0,
									timestamp: "2016-04-05T00:00:00Z",
									count: 1
								},
								{	
									index: 4,
									timestamp: "2016-04-05T04:00:00Z",
									count: 1
								}
							]
						}
					},
					{
						query: {
							interval: 3600,
							end: 1459814400
						},
						expects: {
							length: 11,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T08:00:00Z",
									count: 1
								},
								{	
									index: 10,
									timestamp: "2016-04-05T00:00:00Z",
									count: 1
								}
							]
						}
					},
					{
						query: {
							interval: 3600,
							start: 1456131600,
							end: 1459814400
						},
						expects: {
							length: 10,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T09:00:00Z",
									count: 1
								},
								{	
									index: 9,
									timestamp: "2016-04-05T00:00:00Z",
									count: 1
								}
							]
						}
					},
					{
						query: {
							interval: 7200
						},
						expects: {
							length: 9,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T08:00:00Z",
									count: 2
								},
								{	
									index: 1,
									timestamp: "2016-02-22T10:00:00Z",
									count: 2
								},
								{	
									index: 2,
									timestamp: "2016-02-22T12:00:00Z",
									count: 1
								}
							]
						}
					},
					{
						query: {
							interval: 7200,
							start: 1456131600
						},
						expects: {
							length: 9,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T08:00:00Z",
									count: 2
								},
								{	
									index: 1,
									timestamp: "2016-02-22T10:00:00Z",
									count: 2
								},
								{	
									index: 2,
									timestamp: "2016-02-22T12:00:00Z",
									count: 1
								}
							]
						}
					},
					{
						query: {
							interval: 7200,
							start: 1456135200
						},
						expects: {
							length: 8,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T10:00:00Z",
									count: 2
								},
								{	
									index: 1,
									timestamp: "2016-02-22T12:00:00Z",
									count: 1
								}
							]
						}
					},
					{
						query: {
							interval: 7200,
							start: 1456135200,
							end: 1456138800
						},
						expects: {
							length: 1,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T10:00:00Z",
									count: 2
								}
							]
						}
					}
				];

				const meta = {
					intervals: [
						3600
					],
					timespaces: [
						[
							1456128000,
							1457971200,
							1459814400
						]
					],
					timepoints: this.timepoints
				};

				queries.forEach(query => {
					Object.assign(query.query, baseQuery);

					const result = {
						values: []
					};

					redisStoreUtil.resolveTimepoints(query.query, meta, result);

					assert(result.values.length === query.expects.length);

					query.expects.values.forEach(testValue => {
						const resultValue = result.values[testValue.index];

						assert(resultValue.timestamp === testValue.timestamp);
						assert(resultValue.count === testValue.count);
					});
				});

				done();
			});
		});
	});

	describe("if value", () => {
		this.valueTimepoints = [
			{
				0: {
					count: 10,
					sum: 100
				},
				1: {
					count: 1,
					sum: 1
				},
				2: {
					count: 1,
					sum: 1
				},
				3: {
					count: 1,
					sum: 1
				},
				4: {
					count: 1,
					sum: 1
				}
			},
			{
				0: {
					count: 1,
					sum: 1
				},
				1: {
					count: 1,
					sum: 1
				},
				2: {
					count: 1,
					sum: 1
				},
				3: {
					count: 1,
					sum: 1
				},
				4: {
					count: 1,
					sum: 1
				}
			},
			{
				0: {
					count: 1,
					sum: 1
				},
				1: {
					count: 1,
					sum: 1
				},
				2: {
					count: 1,
					sum: 1
				},
				3: {
					count: 1,
					sum: 1
				},
				4: {
					count: 1,
					sum: 1
				}
			}
		];

		describe("if sum", () => {
			it("sums the correct value limited by various start and ends", done => {
				const baseQuery = {
					type: "v",
					queryType: "sum"
				};

				const queries = [
					{
						query: {},
						expects: {
							average: 4.75
						}
					}
				];

				const meta = {
					intervals: [
						3600
					],
					timespaces: [
						[
							1456128000,
							1457971200,
							1459814400
						]
					],
					timepoints: this.valueTimepoints
				};

				queries.forEach(query => {
					Object.assign(query.query, baseQuery);

					const result = {
						count: 0,
						sum: 0,
						average: 0
					};

					redisStoreUtil.resolveTimepoints(query.query, meta, result);

					assert(result.average === query.expects.average);
				});

				done();
			});
		});

		describe("if range", () => {
			it("returns an array of objects with a timestamp and an average" + 
			   "limited by various start and ends at various interval resolutions", done => {
				const baseQuery = {
					type: "v",
					queryType: "range"
				};

				const queries = [
					{
						query: {
							interval: 3600
						},
						expects: {
							length: 15,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T08:00:00Z",
									average: 10
								}
							]
						}
					},
					{
						query: {
							interval: 7200
						},
						expects: {
							length: 9,
							values: [
								{	
									index: 0,
									timestamp: "2016-02-22T08:00:00Z",
									average: 9.18
								}
							]
						}
					}
				];

				const meta = {
					intervals: [
						3600
					],
					timespaces: [
						[
							1456128000,
							1457971200,
							1459814400
						]
					],
					timepoints: this.valueTimepoints
				};

				queries.forEach(query => {
					Object.assign(query.query, baseQuery);

					const result = {
						values: []
					};

					redisStoreUtil.resolveTimepoints(query.query, meta, result);

					assert(result.values.length === query.expects.length);

					query.expects.values.forEach(testValue => {
						const resultValue = result.values[testValue.index];

						assert(resultValue.timestamp === testValue.timestamp);
						assert(resultValue.average === testValue.average);
					});
				});

				done();
			});
		});
	});
});

describe("objectToArray function", () => {
	it("transforms an object to an array", done => {
		const data = [
			{
				type: "c",
				object: {
					"2016-02-22T08:00:00Z": 10,
					"2016-02-22T09:00:00Z": 15,
					"2016-02-22T10:00:00Z": 20,
				},
				expects: [
					{
						timestamp: "2016-02-22T08:00:00Z",
						count: 10
					},
					{
						timestamp: "2016-02-22T09:00:00Z",
						count: 15
					},
					{
						timestamp: "2016-02-22T10:00:00Z",
						count: 20
					}
				]
			},
			{
				type: "v",
				object: {
					"2016-02-22T08:00:00Z": {
						count: 10,
						sum: 10,
						average: 1
					},
					"2016-02-22T09:00:00Z": {
						count: 100,
						sum: 10,
						average: 0.1
					},
					"2016-02-22T10:00:00Z": {
						count: 20,
						sum: 50,
						average: 2
					},
				},
				expects: [
					{
						timestamp: "2016-02-22T08:00:00Z",
						count: 10,
						sum: 10,
						average: 1
					},
					{
						timestamp: "2016-02-22T09:00:00Z",
						count: 100,
						sum: 10,
						average: 0.1
					},
					{
						timestamp: "2016-02-22T10:00:00Z",
						count: 20,
						sum: 50,
						average: 2
					}
				]
			}
		];

		data.forEach(item => {
			let toArrayer;

			switch (item.type) {
				case "c":
					toArrayer = redisStoreUtil.objectToArrayCounter;
					break;
				case "v":
					toArrayer = redisStoreUtil.objectToArrayValue;
					break;
			}

			const arrayValues = toArrayer(item.object);

			arrayValues.forEach((value, index) => {
				assert(value.timestamp === item.expects[index].timestamp);
				assert(value.count === item.expects[index].count);
			});
		});

		
		done();
	});
});

describe("generateBaseResult", () => {
	it("returns correct result defaults", done => {
		const data = [
			{
				data: {
					type: "c",
					queryType: "sum",
					key: "test"
				},
				expects: {
					count: 0
				}
			},
			{
				data: {
					type: "c",
					queryType: "range",
					key: "test"
				},
				expects: {
					values: {}
				}
			},
			{
				data: {
					type: "v",
					queryType: "sum",
					key: "test"
				},
				expects: {
					count: 0,
					sum: 0,
					average: 0
				}
			},
			{
				data: {
					type: "v",
					queryType: "range",
					key: "test"
				},
				expects: {
					values: {}
				}
			}
		];

		data.forEach(item => {
			const result = redisStoreUtil.generateBaseResult(
				item.data.type, item.data.queryType);

			for (let key in item.expects) {
				if (typeof item.expects[key] === "object") {
					assert(typeof result[key] === typeof item.expects[key]);
				} else {
					assert(result[key] === item.expects[key]);
				}
			}
	
		});

		done();
	});
})