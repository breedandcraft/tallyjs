"use strict";

const moment = require("moment");
const timeUtil = require("./time");

const Validation = {
	validateData: function(data) {
		switch (data.key) {
			case undefined:
				throw new Error("'data.key' must be defined.");
			case "":
				throw new Error("'data.key' must not be empty.");
		}

		if (
			data.interval !== undefined &&
			(isNaN(data.interval) || parseInt(data.interval) !== data.interval)
		) {
			throw new Error("'data.interval' must be a number.");
		} else if (data.interval === undefined || data.interval <= 0) {
			data.interval = 3600;
		}
	},

	validateInput: function(data) {
		this.validateData(data);

		if (data.timestamp !== undefined) {
			timeUtil.validateTimestamp(data.timestamp);
		}

		if (data.count === undefined) {
			data.count = 1;
		}

		if (data.type === "v") {
			if (data.value === undefined) {
				data.value = 1;
			}
		}

		let now;
		switch (typeof data.timestamp) {
			case "string":
				now = moment(data.timestamp).utc().unix();
				break;
			case "number":
				now = data.timestamp;
				break;
			default:
				now = moment().utc().unix();
		}

		data.timestamp = timeUtil.floorTimestamp(now, data.interval);
	},

	validateQuery: function(query) {
		this.validateData(query);

		if (query.start !== undefined) {
			timeUtil.validateTimestamp(query.start);
		}

		if (query.end !== undefined) {
			timeUtil.validateTimestamp(query.end);
		}

		if (query.start !== undefined && typeof query.start === "string") {
			query.start = moment(query.start).utc().unix();
		}

		if (query.end !== undefined && typeof query.end === "string") {
			query.end = moment(query.end).utc().unix();
		}
	}
};

module.exports = Validation;