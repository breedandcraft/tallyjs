"use strict";

const Redis = require("ioredis");
const moment = require("moment");

const timeUtil = require("./time");

module.exports = RedisStore;

function RedisStore(config) {
	// Since config is passed by reference, we'll copy it's properties and add
	// to it for our keyPrefix so that we don't mess with the outside object.
	const redisConfig = Object.assign({}, config);
	redisConfig.keyPrefix = "tally:";

	this.redis = new Redis(redisConfig);

	// [NYI] Check to see if we've set this in a data/config ourselves.
	// If not, call 'config get "hash-max-ziplist-entries"' to get the value
	// then save it.
	this.hashMaxZiplistEntries = 512;

	// To avoid repeated if logic/switches, we construct an object with the
	// functions needed, determined by the query type and type.
	this.queryFunctions = {
		sum: {
			resolve: this.resolveSum.bind(this),
			c: {
				getTimepoints: this.getTimepointsCounter.bind(this),
				resolveData: this.resolveSumCounter
			},
			v: {
				getTimepoints: this.getTimepointsValue.bind(this),
				resolveData: this.resolveSumValue
			}
		},
		range: {
			resolve: this.resolveRange.bind(this),
			c: {
				getTimepoints: this.getTimepointsCounter.bind(this),
				resolveData: this.resolveRangeCounter,
				objectToArray: this.objectToArrayCounter
			},
			v: {
				getTimepoints: this.getTimepointsValue.bind(this),
				resolveData: this.resolveRangeValue,
				objectToArray: this.objectToArrayValue
			}
		}
	};
}

RedisStore.prototype.addData = function(data) {
	const meta = {
		key: data.type + data.key,
		pipeline: this.redis.pipeline()
	};

	// Generate a timespace so that we can bucket the data into hashes.
	meta.timespace = timeUtil.getTimespace(
		data.timestamp,
		data.interval,
		this.hashMaxZiplistEntries
	);

	// Generate a timepoint for the data: A timepoint is the ID in the
	// of the data in the timespace hash.
	meta.timepoint = timeUtil.getTimepoint(
		data.timestamp,
		meta.timespace,
		data.interval
	);

	// The following redis sets are used for indexing, so that we can
	// keep track of the intervals and timespaces a key has.

	// Add the interval key set.
	meta.pipeline.sadd(
		meta.key,
		data.interval
	);

	// Add the timespace to the key+interval set.
	meta.key += data.interval;
	meta.pipeline.sadd(
		meta.key,
		meta.timespace
	);

	meta.key += meta.timespace;

	return meta;
};

RedisStore.prototype.addCounter = function(data) {
	const meta = this.addData(data);

	meta.pipeline.hincrby(
		meta.key,
		meta.timepoint,
		data.count
	);

	return meta.pipeline.exec();
};

RedisStore.prototype.addValue = function(data) {
	const meta = this.addData(data);

	meta.pipeline.hincrby(
		meta.key + "count",
		meta.timepoint,
		data.count
	);

	meta.pipeline.hincrby(
		meta.key + "sum",
		meta.timepoint,
		data.count * data.value
	);

	return meta.pipeline.exec();
};

// [NYI] Need to implement better testing for this.
RedisStore.prototype.getData = function(query) {
	const meta = {};

	return new Promise((resolve, reject) => {

		const result = this.generateBaseResult(query.type, query.queryType);

		// [NYI] The first two trips to redis could be rolled into 1 using
		// scripting (get intervals and timespaces).

		// Get the intervals that a key of this type has.
		this.redis.smembers(query.type + query.key).then(intervals => {
			if (intervals.length === 0) {
				// If we get an empty array then we assume the key does not
				// exist/has no data, so we return the default result object
				// along with an error message to indicate something went
				// wrong.

				if (query.queryType === "range") {
					// If the query type is a range, then we need to return
					// an array since internally we handle it as an object until
					// we need to return it.
					result.values = [];
				}

				result.error = new Error(`${query.key} has no data.`);

				resolve(result);
			} else {
				// If we have intervals, add them to the meta object for use
				// further down the chain.
				meta.intervals = intervals;

				// And then get the timespaces for each of the intervals.
				return this.getTimespaces(query, meta);
			}
		}).then(intervalTimespaces => {
			// Create an empty array for us to keep a track of the timepoints
			// that belong to a timespace (it will be an array of arrays).
			meta.timespaces = [];
			// Add the timespaces for each interval to our meta for use
			// further down the chain.
			meta.intervalTimespaces = intervalTimespaces;

			// Grab the timepoints for each of the timespaces for each of the
			// intervals.
			return this.getTimepoints(query, meta);
		}).then(timepoints => {	
			// Now that we have the data we need, we need to process the
			// timepoints (summing or bucketing into ranges) before we return a
			// result.

			switch (query.type) {
				case "c":
					meta.timepoints = timepoints;
					break;
				case "v":
					meta.timepoints = this.prepareValueTimepoints(
						timepoints
					);
					break;
			}

			this.resolveTimepoints(query, meta, result);

			resolve(result);
		}).catch(err => {
			reject(err);
		});
	});
};

RedisStore.prototype.generateBaseResult = function(type, queryType) {
	let result;

	switch (queryType) {
		case "range":
			result = {
				values: {}
			};
			break;

		case "sum":
			switch (type) {
				case "c":
					result = {
						count: 0
					};

					break;
				case "v":
					result = {
						count: 0,
						sum: 0,
						average: 0
					};

					break;
			}

			break;
	}

	return result;
};

RedisStore.prototype.getTimespaces = function(query, meta) {
	return Promise.all(meta.intervals.map(interval => this.redis.smembers(
		query.type + query.key + interval)));
};

RedisStore.prototype.getTimepoints = function(query, meta) {
	const promises = [];

	meta.intervalTimespaces.forEach((timespaces, index) => {
		meta.timespaces.push([]);

		const timespaceSeconds = (
			meta.intervals[index] * this.hashMaxZiplistEntries
		);

		const timespaceRange = {
			start: timeUtil.floorTimestamp(query.start, timespaceSeconds),
			end: timeUtil.floorTimestamp(query.end, timespaceSeconds)
		};

		timespaces.forEach(timespace => {
			const isInRange = timeUtil.checkTimeIsInRange(
				timespaceRange, 
				timespace
			);

			if (isInRange) {
				const timespaceKey = query.type + query.key +
					meta.intervals[index] + timespace;

				meta.timespaces[index].push(parseInt(timespace));

				// [NYI] This could be optimised by using a pipeline instead
				// of a bunch of promises.

				promises.push.apply(
					promises,
					this.queryFunctions[query.queryType][query.type]
						.getTimepoints(timespaceKey)
				);
			}
		});
	});

	return Promise.all(promises);	
};

RedisStore.prototype.getTimepointsCounter = function(timespaceKey) {
	return [
		this.redis.hgetall(timespaceKey)
	];
};

RedisStore.prototype.getTimepointsValue = function(timespaceKey) {
	return [
		this.redis.hgetall(timespaceKey + "count"),
		this.redis.hgetall(timespaceKey + "sum")
	];
};

// The data structure is different for Counters and Values, so for Values we
// need to manipulate the returned data so that it is an array of objects
// instead of two linked arrays.
RedisStore.prototype.prepareValueTimepoints = function(timepoints) {
	const newTimepoints = [];

	for (let index = 0; index < timepoints.length; index += 2) {
		const combined = {};

		for (let key in timepoints[index]) {
			combined[key] = {
				count: timepoints[index][key],
				sum: timepoints[index + 1][key]
			};
		}

		newTimepoints.push(combined);
	}

	return newTimepoints;
};

RedisStore.prototype.resolveTimepoints = function(query, meta, result) {
	meta.timepointTracker = 0;

	meta.intervals.forEach((interval, index) => {
		meta.currentInterval = interval;

		meta.timespaces[index].forEach((timespace, timespaceIndex) => {
			meta.timespace = timespace;
			meta.timepointTracker = timespaceIndex;

			for (let key in meta.timepoints[meta.timepointTracker]) {
				meta.key = key;

				this.queryFunctions[query.queryType].resolve(query, meta, result);
			}
		});
	});

	// We need to calculate the average for the Value type (at this point we
	// have counts and sums).
	if (query.type === "v") {
		this.calculateAverage(query.queryType, result);
	}

	// Instead of just assuming that the data will be in order, we do an
	// explicit sort if we have timeseries data.
	if (query.queryType === "range" && query.resultType !== "object") {
		result.values = this.queryFunctions[query.queryType][query.type]
			.objectToArray(result.values);

		result.values.sort((a,b) => a.timestamp.localeCompare(b.timestamp));
	}		
};

RedisStore.prototype.resolveSum = function(query, meta, result) {
	const isInRange = timeUtil.checkTimeIsInRange(
		{
			start: timeUtil.floorTimestamp(query.start, meta.currentInterval),
			end: timeUtil.floorTimestamp(query.end, meta.currentInterval)
		},
		timeUtil.resolveTimepoint(
			meta.timespace,
			meta.key,
			meta.currentInterval
		)
	);

	if (isInRange) {
		const timepoint = meta.timepoints[meta.timepointTracker][meta.key];

		this.queryFunctions[query.queryType][query.type]
			.resolveData(result, timepoint);
	}
};

RedisStore.prototype.resolveSumCounter = function(result, timepoint) {
	result.count += parseInt(timepoint);
};

RedisStore.prototype.resolveSumValue = function(result, timepoint) {
	result.count += parseInt(timepoint.count);
	result.sum += parseInt(timepoint.sum);
};

RedisStore.prototype.resolveRange = function(query, meta, result) {
	const flooredTimestamp = timeUtil.floorTimestamp(
		timeUtil.resolveTimepoint(
			meta.timespace,
			meta.key,
			meta.currentInterval
		),
		query.interval
	);

	const isInRange = timeUtil.checkTimeIsInRange(
		{
			start: timeUtil.floorTimestamp(query.start, query.interval),
			end: timeUtil.floorTimestamp(query.end, query.interval)
		},
		flooredTimestamp
	);

	if (isInRange) {
		const timepoint = meta.timepoints[meta.timepointTracker][meta.key];
		const humanTime = moment.unix(flooredTimestamp).utc()
			.format("YYYY-MM-DDTHH:mm:SS") + "Z";

		this.queryFunctions[query.queryType][query.type]
			.resolveData(result, humanTime, timepoint);
	}
};

RedisStore.prototype.resolveRangeCounter = function(result, humanTime, timepoint) {
	if (result.values[humanTime] === undefined) {
		result.values[humanTime] = parseInt(timepoint);
	} else {
		result.values[humanTime] += parseInt(timepoint);
	}
};

RedisStore.prototype.resolveRangeValue = function(result, humanTime, timepoint) {
	if (result.values[humanTime] === undefined) {
		result.values[humanTime] = {
			count: parseInt(timepoint.count),
			sum: parseInt(timepoint.sum)
		};
	} else {
		result.values[humanTime]
			.count += parseInt(timepoint.count);
		result.values[humanTime]
			.sum += parseInt(timepoint.sum);
	}
};

RedisStore.prototype.objectToArrayCounter = function(values) {
	const valuesArray = [];

	for (let key of Object.keys(values)) {
		valuesArray.push({
			timestamp: key,
			count: values[key]
		});
	}

	return valuesArray;
};

RedisStore.prototype.objectToArrayValue = function(values) {
	const valuesArray = [];

	for (let key of Object.keys(values)) {
		valuesArray.push({
			timestamp: key,
			count: values[key].count,
			sum: values[key].sum,
			average: values[key].average
		});
	}

	return valuesArray;
};

// [NYI] Implement test...
RedisStore.prototype.calculateAverage = function(queryType, result) {
	switch (queryType) {
		case "range":
			for (let key of Object.keys(result.values)) {
				result.values[key].average = parseFloat(
					(
						result.values[key].sum /
						result.values[key].count
					).toFixed(2)
				);
			}
			
			break;
		case "sum":
			result.average = parseFloat(
				(result.sum / result.count).toFixed(2)
			);
			break;
	}
};