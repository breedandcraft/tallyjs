"use strict";

const moment = require("moment");

const Time = {
	validateTimestamp: function(timestamp) {
		switch (typeof timestamp) {
			case "string":
				const isValid = moment(
					timestamp,
					"YYYY-MM-DDTHH:mm:ssZ",
					true
				).isValid();

				if (timestamp === "") {
					throw new Error("'timestamp' must not be empty.");
				} else if (isValid === false) {
					throw new Error(
						"'timestamp' must in the following format: " +
						"2016-02-19T03:26:07Z"
					);
				}

				break;
			case "number":
				if (timestamp < 0) {
					throw new Error("'timestamp' must not be 0.");
				}

				break;
		}
	},

	floorTimestamp: function(timestamp, interval) {
		if (timestamp === undefined) {
			return undefined;
		}
		
		return (Math.floor(timestamp / interval) * interval);
	},

	getTimespace: function(timestamp, interval, hashMaxZiplistEntries) {
		return this.floorTimestamp(
			timestamp,
			interval * hashMaxZiplistEntries
		);
	},

	// getTimepoint returns a timepoint based on the timespace and interval
	// combination.
	getTimepoint: function(timestamp, timespace, interval) {
		if (timestamp < timespace) {
			throw new Error("'timestamp' cannot be less than 'timespace'");
		}

		return (timestamp - timespace) / interval;
	},

	// resolveTimepoint returns a unix timestamp of a timepoint for an interval
	// and timespace combination.
	resolveTimepoint: function(timespace, timepoint, interval) {
		return (timespace + (interval * timepoint));
	},

	checkTimeIsInRange: function(data, timestamp) {
		return (data.start === undefined || timestamp >= data.start) &&
			(data.end === undefined || timestamp <= data.end);
	}
};

module.exports = Time;