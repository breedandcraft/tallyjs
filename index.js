"use strict";

const validateionUtil = require("./utils/validation");
const RedisStoreUtil = require("./utils/redisStore");

// [NOTE] Ideas for 'cold' data.
// We should wharehouse data after it is more than 1 timespace old.

// We should read from both data sources and treat the non-Redis data as the
// 'truth' so that we do not get doubling up of data.

// To archive we should watch and then get a timespace hash.
// We then insert/update into the 'cold' storage and attempt to delete the hash
// and timespace from the interval set using a multi/exec.
// If the hash hasn't changed it will succeed.
// If it fails then we decrement all values of the hash with the values we have.

// Do a check to see how many timespaces remain for an interval. If 0, watch the
// set and attempt to remove the interval from the interval set. If fail, don't
// worry, it just means more data has been added to the interval.

// Repeat same process for interval set, if empty watch and attempt to delete
// set.

// We would most likely need to kick out the resolve* functions from redisStore
// to a resolve module.

module.exports = Tally;

function Tally(config) {
	this.redisStoreUtil = new RedisStoreUtil(config);
}

Tally.prototype.addCounter = function(data) {
	data.type = "c";

	validateionUtil.validateInput(data);

	return this.redisStoreUtil.addCounter(data);
};

Tally.prototype.getCounterSum = function(query) {
	query.type = "c";
	query.queryType = "sum";

	validateionUtil.validateQuery(query);

	return this.redisStoreUtil.getData(query);
};

Tally.prototype.getCounterRange = function(query) {
	query.type = "c";
	query.queryType = "range";

	validateionUtil.validateQuery(query);

	return this.redisStoreUtil.getData(query);
};

Tally.prototype.addValue = function(data) {
	data.type = "v";

	validateionUtil.validateInput(data);

	return this.redisStoreUtil.addValue(data);
};

Tally.prototype.getValueSum = function(query) {
	query.type = "v";
	query.queryType = "sum";

	validateionUtil.validateQuery(query);

	return this.redisStoreUtil.getData(query);
};

Tally.prototype.getValueRange = function(query) {
	query.type = "v";
	query.queryType = "range";

	validateionUtil.validateQuery(query);

	return this.redisStoreUtil.getData(query);
};